import subprocess
import optparse
import re

def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--interface', dest='interface', help="Interface to change its MAC address")
    parser.add_option('-m', '--mac', dest='new_mac', help="New MAC address")
    (options, arguments) = parser.parse_args()
    if not options.interface:
        parser.error('[-] Please specify a interface i.e. wlan0 etc. For information please use --help or -h')
    elif not options.new_mac:
        parser.error('[-] Please specify new MAC i.e. 00:00:00:00:00:00 For information please use --help or -h')
    return options

def change_mac(interface, new_mac):
    print("[+] Changing MAC address for " + interface + 'to ' + new_mac)
    subprocess.call(['ifconfig', interface, 'down'])
    subprocess.call(['ifconfig', interface, 'hw', 'ether', new_mac])
    subprocess.call(['ifconfig', interface, 'up'])